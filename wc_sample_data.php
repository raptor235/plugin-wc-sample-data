<?php

// Plugin Name: Sample Data

class import_sample{

	var $sample;

	function import_sample(){
		ob_start();

		require_once('sample_data.txt');
		require_once('phpQuery-onefile.php');


		$this->sample = ob_get_contents();
		ob_end_clean();


		$this->sample = explode('.',$this->sample);
	}

	function import_sample_data($data){
		mlog("import_sample_data");

		foreach ($data as $key => $import_set) {
			// mlog($import_set);
			for ($i=0; $i < $import_set['count']; $i++) {
				// mlog("Creating sample ($i) -> " . $import_set['post_type']);
				$data = array(
					'post_type' => $import_set['post_type'],
					'post_title' => $this->get_text(3,8),
					'post_content' => $this->get_text(8,10,3),
					'post_status' => $import_set['post_status'],
					);

				$ID = wp_insert_post( $data );
				mlog('$ID',$ID);

				if ($import_set['thumb'])
					$img = $this->get_image($ID);

				if ($import_set['meta']) {
					foreach ($import_set['meta'] as $mkey => $mvalue) {
						update_post_meta($ID,$mkey,$mvalue);
					}
				}
				update_post_meta($ID,'sample_content',true);

			}
		}
		die('Done Importing');

	}

	function get_image($post_id){
		$tmp = file_get_contents('http://wallbase.cc/index.php/random/index');
		$doc = phpQuery::newDocumentHTML($tmp);

		foreach(pq('.thlink') as $li) {
        // iteration returns PLAIN dom nodes, NOT phpQuery objects
        // so you NEED to wrap it within phpQuery, using pq();
			$url = pq($li)->attr('href');

			$tmp2 = file_get_contents($url);
			$doc2 = phpQuery::newDocumentHTML($tmp2);
			$big = $doc2['#bigwall']->html();
			$big = explode('src="',$big);
			$big = explode('"',$big[1]);
			$big = $big[0];
			$this->save_thumbnail($big,$post_id);
			return;
		}
	}

	function save_thumbnail($src,$postID){
		mlog("save_thumbnail");
		mlog('$postID',$postID);
        // return false;
		if (strpos($src,'.jpg') !== false) {
			$ext = ".jpg";
		} elseif (strpos($src,'.gif') !== false) {
			$ext = ".gif";
		} elseif (strpos($src,'.png') !== false) {
			$ext = ".png";
		}

		$upload_dir = wp_upload_dir();
		$remoteimage = @file_get_contents($src);
		if ($remoteimage) {
			$tmpFile = $upload_dir['path'] . '/sample_' . $postID . $ext;
			file_put_contents($tmpFile,$remoteimage);

			$wp_filetype = wp_check_filetype(basename($tmpFile), null );
			$attachment = array(
				'post_mime_type' => $wp_filetype['type'],
				'post_title' => preg_replace('/\.[^.]+$/', '', basename($tmpFile)),
				'post_content' => '',
				'post_status' => 'inherit'
				);
			$attach_id = wp_insert_attachment( $attachment, $tmpFile, $postID );
			require_once(ABSPATH . "wp-admin" . '/includes/image.php');
			$attach_data = wp_generate_attachment_metadata( $attach_id, $tmpFile );
			add_post_meta($postID, '_thumbnail_id', $attach_id, TRUE);
			wp_update_attachment_metadata( $attach_id,  $attach_data );
		}
	}

	function get_text($min_words = 3, $max_words = 10, $combine = null){
		$found = false;
		// mlog("get_text");

		$count = 0;
		$combined_text;

		while ($found == false || $combine > 0) {
			$count++;
			$rand_keys = array_rand($this->sample);
			$text = $this->sample[$rand_keys];
			$text_arr = explode(' ',$text);
			// mlog('count($text_arr)',count($text_arr));
			if (count($text_arr) > $min_words && count($text_arr) < $max_words){
				if ($max_length && (strlen($text) < $max_length)) {
					# code...
					$found = $text;
				} elseif (!$max_length) {
					$found = $text;
				}
			}
			if ($found && $combine > 0) {
				$combined_text .= $text . "\n\r";
				$combine--;
			}
			if ($count > 50) die('Cant find');

		}
		// mlog('$combined_text',$combined_text);
		// mlog('$text',$text);

		if ($combined_text) return $combined_text;
		return $text;

	}

	function flush_buffers(){
		ob_end_flush();
		ob_flush();
		flush();
		ob_start();
	}

}

$data = array(
	array(
		'post_type' => 'entry',
		'post_status' => 'publish',
		'count' => 3,
		'thumb' => true,
		'meta' => array(
			'entry_type' => 'video',
			'status' => 'Approved'
			)
		)
	);

if (isset($_GET['import_sample'])) {
	# code...
	$import_sample_data = new import_sample();
	$import_sample_data->	import_sample_data($data);
}


$sample_wp_all = '<p>OK, so images can get quite complicated as we have a few variables to work with! For example the image below has had a caption entered in the WordPress image upload dialog box, this creates a [caption] shortcode which then in turn wraps the whole thing in a <code>div</code> with inline styling! Maybe one day they\'ll be able to use the <code>figure</code> and <code>figcaption</code> elements for all this. Additionally, images can be wrapped in links which, if you\'re using anything other than <code>color</code> or <code>text-decoration</code> to style your links can be problematic.</p>
<div id="attachment_28" class="wp-caption alignnone" style="width: 510px"><a href="#"><img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_large.png" alt="Your Alt Tag" title="bmxisbest" width="500" height="300" class="size-large wp-image-28"></a><p class="wp-caption-text">This is the optional caption.</p></div>
<p>The next issue we face is image alignment, users get the option of <em>None</em>, <em>Left</em>, <em>Right</em> &amp; <em>Center</em>. On top of this, they also get the options of <em>Thumbnail</em>, <em>Medium</em>, <em>Large</em> &amp; <em>Fullsize</em>. You\'ll probably want to add floats to style the image position so important to remember to clear these to stop images popping below the bottom of your articles.</p>
<img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_medium.png" alt="Your Alt Title" title="Your Title" width="300" height="200" class="alignright size-medium wp-image-28">
<img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" alt="Your Alt Title" title="Your Title" width="150" height="150" class="alignleft size-thumbnail wp-image-28">
<img class="aligncenter size-medium wp-image-28" title="Your Title" src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_medium.png" alt="Your Alt Title" width="300" height="200">
<img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_full.png" alt="Your Alt Title" title="Your Title" width="840" height="300" class="alignnone size-full wp-image-28">
<p>Additionally, to add further confusion, images can be wrapped inside paragraph content, lets test some examples here.<img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_medium.png" alt="Your Alt Title" title="Your Title" width="300" height="200" class="alignright size-medium wp-image-28">
Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean lacinia bibendum nulla sed consectetur.<img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" alt="Your Alt Title" title="Your Title" width="150" height="150" class="alignleft size-thumbnail wp-image-28">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean lacinia bibendum nulla sed consectetur.<img src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" alt="Your Alt Title" title="Your Title" width="150" height="150" class="aligncenter size-thumbnail wp-image-28">Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla. Aenean lacinia bibendum nulla sed consectetur.</p>
<p>And then... Finally, users can insert a WordPress [gallery], which is kinda ugly and comes with some CSS stuck into the page to style it (which doesn\'t actually validate, nor does the markup for the gallery). The amount of columns in the gallery is also changable by the user, but the default is three so we\'ll work with that for our example with an added fouth image to test verticle spacing.</p>
<style type="text/css">#gallery-1{margin:auto;}#gallery-1 .gallery-item{float:left;margin-top:10px;text-align:center;width:33%;}#gallery-1 img{border:2px solid #cfcfcf;}#gallery-1 .gallery-caption{margin-left:0;}</style>
<div id="gallery-1" class="gallery galleryid-1 gallery-columns-3 gallery-size-thumbnail"><dl class="gallery-item">
<dt class="gallery-icon">
<a href="#" title="Your Title"><img width="150" height="150" src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" class="attachment-thumbnail" alt="Your Alt Title" title="Your Title"></a>
</dt></dl><dl class="gallery-item">
<dt class="gallery-icon">
<a href="#" title="Your Title"><img width="150" height="150" src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" class="attachment-thumbnail" alt="Your Alt Title" title="Your Title"></a>
</dt></dl><dl class="gallery-item">
<dt class="gallery-icon">
<a href="#" title="Your Title"><img width="150" height="150" src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" class="attachment-thumbnail" alt="Your Alt Title" title="Your Title"></a>
</dt></dl><br style="clear: both"><dl class="gallery-item">
<dt class="gallery-icon">
<a href="#" title="Your Title"><img width="150" height="150" src="http://www.wpfill.me.s3-website-us-east-1.amazonaws.com/img/img_thumb.png" class="attachment-thumbnail" alt="Your Alt Title" title="Your Title"></a>
</dt></dl>
<br style="clear: both;">
</div>
<table>
<thead>
<tr>
<th>Table Head Column One</th>
<th>Table Head Column Two</th>
<th>Table Head Column Three</th>
</tr>
</thead>
<tfoot>
<tr>
<td>Table Footer Column One</td>
<td>Table Footer Column Two</td>
<td>Table Footer Column Three</td>
</tr>
</tfoot>
<tbody>
<tr>
<td>Table Row Column One</td>
<td>Short Text</td>
<td>Testing a table cell with a longer amount of text to see what happens, you\'re not using tables for site layouts are you?</td>
</tr>
<tr>
<td>Table Row Column One</td>
<td>Table Row Column Two</td>
<td>Table Row Column Three</td>
</tr>
<tr>
<td>Table Row Column One</td>
<td>Table Row Column Two</td>
<td>Table Row Column Three</td>
</tr>
<tr>
<td>Table Row Column One</td>
<td>Table Row Column Two</td>
<td>Table Row Column Three</td>
</tr>
<tr>
<td>Table Row Column One</td>
<td>Table Row Column Two</td>
<td>Table Row Column Three</td>
</tr>
</tbody>
</table>
<ol>
<li>Ordered list item one.</li>
<li>Ordered list item two.</li>
<li>Ordered list item three.</li>
<li>Ordered list item four.</li>
<li>By the way, Wordpress does not let you create nested lists through the visual editor.</li>
</ol>
<ul>
<li>Unordered list item one.</li>
<li>Unordered list item two.</li>
<li>Unordered list item three.</li>
<li>Unordered list item four.</li>
<li>By the way, Wordpress does not let you create nested lists through the visual editor.</li>
</ul>
<blockquote>
Currently WordPress blockquotes are just wrapped in blockquote tags and have no clear way for the user to define a source. Maybe one day they\'ll be more semantic (and easier to style) like the version below.
</blockquote>
<blockquote cite="http://html5doctor.com/blockquote-q-cite/">
<p>HTML5 comes to our rescue with the footer element, allowing us to add semantically separate information about the quote.</p>
<footer>
<cite>
<a href="http://html5doctor.com/blockquote-q-cite/">Oli Studholme, HTML5doctor.com</a>
</cite>
</footer>
</blockquote>
<h1>Level One Heading</h1>
<h2>Level Two Heading</h2>
<h3>Level Three Heading</h3>
<h4>Level Four Heading</h4>
<h5>Level Five Heading</h5>
<h6>Level Six Heading</h6>
<p>This is a standard paragraph created using the WordPress TinyMCE text editor. It has a <strong>strong tag</strong>, an <em>em tag</em> and a <del>strikethrough</del> which is actually just the del element. There are a few more inline elements which are not in the WordPress admin but we should check for incase your users get busy with the copy and paste. These include <cite>citations</cite>, <abbr title="abbreviation">abbr</abbr>, bits of <code>code</code> and <var>variables</var>, <q>inline quotations</q>, <ins datetime="2011-12-08T20:19:53+00:00">inserted text</ins>, text that is <s>no longer accurate</s> or something <mark>so important</mark> you might want to mark it. We can also style subscript and superscript characters like C0<sub>2</sub>, here is our 2<sup>nd</sup> example. If they are feeling non-semantic they might even use <b>bold</b>, <i>italic</i>, <big>big</big> or <small>small</small> elements too.&nbsp;Incidentally, these HTML4.01 tags have been given new life and semantic meaning in HTML5, you may be interested in reading this <a title="HTML5 Semantics" href="http://csswizardry.com/2011/01/html5-and-text-level-semantics">article by Harry Roberts</a> which gives a nice excuse to test a link.&nbsp;&nbsp;It is also worth noting in the "kitchen sink" view you can also add <span style="text-decoration: underline;">underline</span>&nbsp;styling and set <span style="color: #ff0000;">text color</span> with pesky inline CSS.</p>
<p style="text-align: left;">Additionally, WordPress also sets text alignment with inline styles, like this left aligned paragraph.&nbsp;Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p>
<p style="text-align: right;">This is a right aligned paragraph.&nbsp;Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p>
<p style="text-align: justify;">This is a justified paragraph.&nbsp;Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p>
<p style="padding-left: 30px;">Finally, you also have the option of an indented paragraph.&nbsp;Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Cras mattis consectetur purus sit amet fermentum.</p> <p>And last, and by no means least, users can also apply the <code>Address</code> tag to text like this:</p> <address>123 Example Street,
Testville,
West Madeupsburg,
CSSland,
1234</address> <p>...so there you have it, all our text elements</p>';



?>